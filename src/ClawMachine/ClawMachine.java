package ClawMachine;

public class ClawMachine {

    private final int MACHINE_SIZE_X = 12;
    private final int MACHINE_SIZE_Y = 6;

    private final double PRIZE_FREQUENCY = 0.3;
    private String[][] prizeGrid;

    private ClawPosition clawPosition;

    public ClawMachine() {
        clawPosition = new ClawPosition(
                MACHINE_SIZE_X/2,
                MACHINE_SIZE_Y/2,
                true
                );

        prizeGrid = new String[MACHINE_SIZE_X][MACHINE_SIZE_Y];

        for(int x = 0; x < MACHINE_SIZE_X; x++){
            for(int y = 0; y < MACHINE_SIZE_Y; y++){
                if(Math.random() < PRIZE_FREQUENCY){
                    prizeGrid[x][y] = "PRIZE";
                }
            }
        }
    }

    //Methods below here make up the Claw Machine's API
    //The API is therefore NOT "the claw machine" in its entirety.
    //The constructor, the ClawPosition class, etc - these are internal parts of the CLaw Machine.
    //They are not the API itself.
    //The API is just the "controls" (interface - in the user interface sense - not the Java interface sense)

    //An API is just some controls that a client can use to get the underlying system to behave in a defined way.
    //The underlying system in this case is the ClawMachine class. The API is just these public methods.

    //Just like how a claw machine's "User Interface" exposes some controls (buttons, dials, levers) to the user (a human being)
    //and produces defined behaviors in the underlying system (moving the claw) according to a contract (the instructions)
    //...
    //A claw machine's "Application Programming Interface" exposes some controls (methods, endpoints, objects) to the user (a piece of code)
    //and produces defined behaviors in the underlying system (moving the claw) according to a contract (the API specification/documentation)
    public ClawPosition moveClawForward(){
        int newPositionY = clawPosition.getY() + 1;
        if(newPositionY <= MACHINE_SIZE_Y){
            clawPosition.setY(newPositionY);
        }
        else {
            throw new IllegalArgumentException("Claw cannot move further in this direction.");
        }
        return clawPosition;
    }

    public ClawPosition moveClawBackward(){
        int newPositionY = clawPosition.getY() - 1;
        if(newPositionY >= 0){
            clawPosition.setY(newPositionY);
        }
        else {
            throw new IllegalArgumentException("Claw cannot move further in this direction.");
        }
        return clawPosition;
    }

    public ClawPosition moveClawLeft(){
        int newPositionX = clawPosition.getX() - 1;
        if(newPositionX >= 0) {
            clawPosition.setX(newPositionX);
        }
        else {
            throw new IllegalArgumentException("Claw cannot move further in this direction.");
        }
        return clawPosition;
    }

    public ClawPosition moveClawRight(){
        int newPositionX = clawPosition.getX() + 1;
        if(newPositionX <= MACHINE_SIZE_X) {
            clawPosition.setX(newPositionX);
        }
        else {
            throw new IllegalArgumentException("Claw cannot move further in this direction.");
        }
        return clawPosition;
    }

    public String dropClaw(){
        clawPosition.setRaised(false);
        return prizeGrid[clawPosition.getX()][clawPosition.getY()];
    }
}
