package ClawMachine;

public class ClawPosition {

    private int x;
    private int y;
    boolean raised;

    public ClawPosition(int x, int y, boolean raised) {
        this.x = x;
        this.y = y;
        this.raised = raised;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isRaised() {
        return raised;
    }

    public void setRaised(boolean raised) {
        this.raised = raised;
    }
}
